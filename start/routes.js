'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/register', 'AuthController.register')
Route.post('/login', 'AuthController.login')

Route.group(() => {
  Route.get('/todos', 'TodoController.index')
  Route.post('/todos', 'TodoController.store')
  Route.put('/todos/:id', 'TodoController.update')
  Route.delete('/todos/:id', 'TodoController.destroy')
  Route.put('/todos/:id/complete', 'TodoController.markAsCompleted')
}).middleware(['auth'])
