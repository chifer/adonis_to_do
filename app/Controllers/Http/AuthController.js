'use strict'

const User = use('App/Models/User')

class AuthController {
  async register({ request, auth }) {
    const userData = request.only(['username', 'email', 'password'])
    const user = await User.create(userData)
    return await auth.generate(user)
  }

  async login({ request, auth }) {
    const { email, password } = request.all()
    return await auth.attempt(email, password)
  }
}

module.exports = AuthController
