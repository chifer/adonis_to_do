'use strict'

const {json} = require("@adonisjs/bodyparser/config/bodyParser");
const Todo = use('App/Models/Todo')

class TodoController {
  async index({ auth }) {
    const user = await auth.getUser()
    return {
      success: true,
      data: await user.todos().fetch()
    }
  }

  async store({ request, auth }) {
    const user = await auth.getUser()
    const { title, description } = request.all()
    if (title) {
      const todo = new Todo()
      todo.fill({
        title,
        description
      })
      await user.todos().save(todo)
      return {
        success: true,
        data: todo
      }
    } else {
      return {
        success: false,
        message: 'title is required'
      }
    }
  }

  async update({ params, request }) {
    const { id } = params
    const todo = await Todo.find(id)

    if (todo) {
      const { title, description, completed } = request.all()
      todo.merge({
        title,
        description,
        completed
      })
      await todo.save()

      return {
        success: true,
        data: todo
      }
    } else {
      return {
        success: false,
        message: 'Todo not found'
      }
    }
  }

  async destroy({ params }) {
    const { id } = params
    const todo = await Todo.find(id)
    let resultText = 'Todo not found'
    if (todo) {
      await todo.delete()
      resultText = 'Todo deleted successfully'
      return {
        success: true,
        data: { message: resultText }
      }
    }
    return {
      success: true,
      data: { message: resultText }
    }
  }

  async markAsCompleted({ params }) {
    const { id } = params
    const todo = await Todo.find(id)

    if (!todo) {
      return {
        success: false,
        message: 'Todo not found'
      }
    }

    todo.completed = true
    await todo.save()

    return {
      success: true,
      data: todo
    }
  }
}

module.exports = TodoController
