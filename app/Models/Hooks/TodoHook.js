'use strict'

const { v4: uuidv4 } = require('uuid')

const TodoHook = exports = module.exports = {}

TodoHook.uuid = async (todo) => {
  todo.id = uuidv4()
}
