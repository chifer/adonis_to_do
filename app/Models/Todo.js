'use strict'

const Model = use('Model')

class Todo extends Model {
  static boot() {
    super.boot()

    this.addHook('beforeCreate', 'TodoHook.uuid')
  }

  static get primaryKey() {
    return 'id'
  }

  static get incrementing() {
    return false
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Todo
