'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CreateTodosSchema extends Schema {
  up () {
    this.create('todos', (table) => {
      table.uuid('id').primary()
      table.string('title').notNullable()
      table.text('description')
      table.boolean('completed').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('todos')
  }
}

module.exports = CreateTodosSchema
